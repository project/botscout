CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The BotScout module will check new users by there ip, name, email, or any 
combination of the three to see if they are a known bot. This module checks the 
information against a database of known bots. If the user is found in the 
database they are not allowed to create an account. If any other form is also 
being checked they will not be allowed to submit the form if their data 
matches information found in the database, this includes the login form.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/projects/botscout

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/botscout


REQUIREMENTS
------------
This module requires the following modules:

 No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

 * Module configuration in Administration » Config » System » Botscout:


   - Enable IP filtering


     Enables/disables checking new users by their ip address.


   - Enable username filtering


     Enables/disables checking new users by username.


   - Enable email filtering


     Enables/disables checking new users by email address.


   - Form choices


    You have the choice of letting BotScout protect the registration form which is the default, 
    the registration form and the contact form, or all forms on your site.
    When you choose all forms only forms with a username or email can be checked unless you choose
    to filter by IP address, which is not recommeded. Forms must have field names matching below.

    user: user,name,or username
    email: email, or mail

   - API key


    The botscout API limits you to 20 checks per day without an API key.
    Due to new features this module requires you to get an API key to prevent user lockout.
    Please visit www.botscout.com to get a free API key.


MAINTAINERS
-----------

Current maintainers:
 * Jonathan Langlois (acetolyne) - https://www.drupal.org/user/1036768
